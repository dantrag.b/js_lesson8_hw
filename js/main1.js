// Створити клас Car , Engine та Driver.
// Клас Driver містить поля - ПІБ, стаж водіння.
// Клас Engine містить поля – потужність, виробник.
// Клас Car містить поля – марка автомобіля, клас автомобіля, вага, водій типу Driver, мотор типу Engine. 
// Методи start(), stop(), turnRight(), turnLeft(), які виводять на друк: "Поїхали", "Зупиняємося", "Поворот праворуч" або "Поворот ліворуч". 
// А також метод toString(), який виводить повну інформацію про автомобіль, її водія і двигуна.

// Створити похідний від Car клас - Lorry (вантажівка), що характеризується також вантажопідйомністю кузова.
// Створити похідний від Car клас - SportCar, який також характеризується граничною швидкістю.
// https://storage.googleapis.com/www.examclouds.com/oop/car-ierarchy.png



class Person {
  #age;
  constructor(fullName, age = undefined) {
    this.fullName = fullName;
    this.personAge = age;
  }
  set personAge(age) {
    this.#age = age;
  }
  get personAge() {
    return this.#age;
  }
}

class Driver extends Person {
  constructor(fullName, expirience) {
    super(fullName);
    this.expirience = expirience;
  }
}

class Engine {
  constructor(power, manufacturer) {
    this.power = power;
    this.manufacturer = manufacturer;
  }
}

class Car {
  constructor(model, carCalss, weight, driver, engine) {
    this.model = model;
    this.carCalss = carCalss;
    this.weight = weight;
    this.driver = driver;
    this.engine = engine;
  }
  star() {
    return console.log("Engine statr");
  }
  stop() {
    return console.log("Engine stop");
  }
  turnLeft() {
    return console.log("Carn turning left");
  }
  turnRight() {
    return console.log("Car turning right");
  }
  toString() {
    document.write(`Модель: ${this.model}<br> Класс: ${this.carCalss}<br> Вес: ${this.weight}кг <br> Двигатель фирмы: ${this.engine.manufacturer}<br> Мощность: ${this.engine.power}л.с.<br> Зарегистрирована на: ${this.driver.fullName}<br>`);
  }
}

class Lorry extends Car {
  constructor(model, carCalss, weight, driver, engine, loadCapacity) {
    super(model, carCalss, weight, driver, engine);
    this.loadCapacity = loadCapacity;
  }
  toString() {
    super.toString();
    document.write(`Грузоподъёмность: ${this.loadCapacity}<br><br><br><br>`);
  }
}

class SportCar extends Car {
  constructor(model, carCalss, weight, driver, engine, maxSpeed) {
    super(model, carCalss, weight, driver, engine);
    this.maxSpeed = maxSpeed;
  }
  toString() {
    super.toString();
    document.write(`Максимальная скорость: ${this.maxSpeed}<br><br><br><br>`);
  }
}


const pers1 = new Person("Ivan", 33);

console.log(pers1);

const driver1 = new Driver("Stepan", 12);
const driver2 = new Driver("Nick", 22);

console.log(driver1, driver2);


const engine1 = new Engine(560, 'Cosworth');
const engine2 = new Engine(320, 'Ford');

console.log(engine1, engine2);

const lorry1 = new Lorry('Mercedes', 'Heavy lifter', 10234, driver1, engine1, 5000);
const lorry2 = new Lorry('Volvo', 'Tractor', 8694, driver2, engine2, 2500);

console.log(lorry1, lorry2);


const sportCar1 = new SportCar('Lancia', 'coupe', 14025, driver1, engine1, 220);
const sportCar2 = new SportCar('Ferrari', 'roadster', 1290, driver2, engine2, 330);

console.log(sportCar1, sportCar2);


lorry1.toString();
sportCar2.toString();

